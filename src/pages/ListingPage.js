import React, { useEffect } from 'react';
import Card from '../components/Card';
import feed from '../../feed/data.json';

const ListingPage = () => {
	useEffect(() => {
		scrollTo(0, 0);
	}, []);

	return (
		<div className="card-container">
			{feed.map((res, key) => {
				return <Card key={key} item={res} />;
			})}
		</div>
	);
};

export default ListingPage;
